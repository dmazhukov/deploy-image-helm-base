#!/usr/bin/env bash

# Check the environment to be sure the user can actually build and use the
# tooling included with this repository.

problem_list=""
runtime_warning=""

merge_problem()
{
  problem="$1"

  echo -en "${problem_list}\n${problem}"
}

add_problem() {
    problem="$1"

    if [ -z "${problem_list}" ]; then
        problem_list="$(echo -e "${problem}")"
    else
        problem_list=$(merge_problem "${problem}")
    fi
}

REQUIRED_COMMANDS="git gcloud kubectl helm mpdev podman docker ruby"

# environment configuration must exist
GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

if [ ! -f "${GL_MP_ENV_CONFIG}" ]; then
    echo "Missing ${GL_MP_ENV_CONFIG}"
    exit 1
fi

echo "[START] Checking environment for tools necessary to build a GitLab GKE Marketplace release..."

found_runtime="no"
active_runtime="no"
for app in $REQUIRED_COMMANDS; do
    found_it="Found"
    if ! command -v "${app}" > /dev/null 2>&1; then
        found_it="Not Found"
        if [ "${app}" != "docker" ] && [ "${app}" != "podman" ]; then
            add_problem "${app} is not installed"
        fi
    else
        if [ "${app}" = "docker" ] || [ "${app}" = "podman" ]; then
            found_runtime="yes"
            if command -v "${app}" ps > /dev/null 2>&1; then
                active_runtime="yes"
                if [ "${app}" = "podman" ]; then
                  runtime_warning="Marketplace Tools invoke 'docker', alias podman to docker for compatibility"
                fi
            fi
        fi
    fi
    echo "Checking for ${app}...${found_it}"
done

# container runtime is required
if [ "${found_runtime}" = "no" ]; then
    add_problem "Install a container runtime"
fi

if [ "${active_runtime}" = "no" ]; then
    add_problem "Container runtime service is not active"
fi

if kubectl get crd --all-namespaces|grep -q 'applications.app.k8s.io' > /dev/null 2>&1; then
    echo "The GKE Marketplace Application CRD has been applied"
else
    add_problem "Missing the GKE Marketplace Application CRD"
fi

# make sure the right yq is installed
# behavioral test for posterity in case this ever breaks
#yq_test=$(echo '{"test": "works"}'|yq -r ".| .test" 2>/dev/null)
if ! yq --help | grep -q kislyuk; then
    add_problem "Expect python version of yq"
fi

if [ -n "${runtime_warning}" ]; then
    echo "[WARNING] ${runtime_warning}"
fi

if [ -z "${problem_list}" ]; then
    echo "[SUCCESS] Environment is ready to build GitLab GKE Marketplace"
else
    echo "[PROBLEMS]"
    echo "${problem_list}"
    echo "[FAILURE] Resolve above issues to build GitLab GKE Marketplace"
    exit 1
fi
