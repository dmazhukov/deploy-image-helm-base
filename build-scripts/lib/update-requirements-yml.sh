#!/usr/bin/env bash
# update-requirements-yml
# Script to update the GitLab version number in the requirements.yaml
# Optional takes an argument to set the version if needed
set -e

GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

append_partial() {
    partial="$1"

    [ -n "${partial}" ] || display_failure "append_partial expects a filepath"
    [ -f "${partial}" ] || display_failure "No such file ${partial}"

    if cat "${partial}" >> "${GL_MP_REQUIREMENTS_FILE}"; then
        display_success "Injected ${partial} to requirements"
    else
        display_failure "Injection of ${partial} failed"
    fi
}

################################################################################
# Main Script
################################################################################
if [ -n "$1" ]; then
    version_number="$1"
else
    display_failure "Must pass chart version number"
fi

remove_file "${GL_MP_REQUIREMENTS_FILE}"
remove_file "${GL_MP_REQUIREMENTS_LOCK_FILE}"

echo "dependencies:" > "${GL_MP_REQUIREMENTS_FILE}"

partials_path="${GL_MP_TEMPLATES}/requirements/"

# assume GitLab upstream chart version written unless second argument passed
if [ -z "$2" ]; then
    append_partial "${partials_path}gitlab-reqs.yaml"
    add_dead_file "${GL_MP_REQUIREMENTS_FILE}-e"

    pattern="s/\$GL_RELEASE_VERSION/${version_number}/"
    if sed -i -e "${pattern}" "${GL_MP_REQUIREMENTS_FILE}"; then
        display_success "GitLab Chart required version set to ${version_number}"
    else
        display_failure "Failed setting GitLab Chart version to ${version_number}"
    fi
fi

append_partial "${partials_path}gke-marketplace-reqs.yaml"

display_success "requirements.yaml built succesfully"
